#ifndef _CACHE_H_
#define _CACHE_H_
#include "instruction.h"
//#include "stages.h"
//#include "cpu.h"
//#include "cache_impl.hh"
#include <deque>
#include <math.h> 	// ADDED: used for the logarithm function
#include <map>		// ADDED: used for Stride prefetcher

extern uint32_t cache_size;	//in bytes
extern int prefetcher_type;

//You do not need to parameterize these two.
extern uint32_t cache_assoc;
extern uint32_t cacheline_size;

// ADDED CODE START

// structure for a member of the prefetch queue
struct PrefetchReq {
	uint32_t addr;			// prefetched block address
	uint32_t readyCycle;	// cycle when the prefetch will finish
};

// structure for Stride prefetcher - values of RPT (indexed by PC)
struct RPTVal {
	uint32_t addr;
	int stride;
};

class CacheBlk {
public:
	bool valid;
	bool dirty;
	bool fromPref;	// flag for prefetch policy 2, indicating whether the block was prefetched
	uint32_t tag;
	uint32_t lastAccessed;

	CacheBlk()
	{
		valid = false;
		dirty = false;
		fromPref = false;
		tag = 0;
		lastAccessed = 0;
	}

	bool isValid() {
		return valid;
	};

	bool isDirty() {
		return dirty;
	};

	void markDirty() {
		dirty = true;
	};
};

class SimpleCache {
public:
	uint32_t tagsize;
	uint32_t indexsize;
	uint32_t assoc;
	CacheBlk* blocks;

	SimpleCache(uint32_t cache_size, uint32_t cache_assoc, uint32_t cacheline_size)
	{
		int blockCount = cache_size/cacheline_size;	// get total number of blocks in cache
		blocks = new CacheBlk[blockCount];			// allocate space for all blocks
		indexsize = log2(blockCount/cache_assoc);	// get the size of index
		tagsize = 32-log2(cacheline_size)-indexsize;// get the size of tag
		assoc = cache_assoc;						// store associativity
	}

	~SimpleCache() {
		delete[] blocks;
	}

	// accesses the set based on index, compares tags
	CacheBlk* access(uint32_t addr, uint32_t cycleCount, bool alsoUpdating) {

		uint32_t tagFromAddr = addr>>(32-tagsize);					// compute tag of the address
		uint32_t indexFromAddr = (addr<<(tagsize))>>(32-indexsize);	// compute index of the address

		// iterate through specified set and compare tags; if match, update lastAccessed and return that block
		for (uint32_t i=0; i<assoc; i++) {
			if (blocks[i+indexFromAddr*assoc].tag == tagFromAddr) {
				if (alsoUpdating) blocks[i+indexFromAddr*cache_assoc].lastAccessed = cycleCount;	// do not set lastAccessed in prefetch check (alsoUpdating=false)
				return &blocks[i+indexFromAddr*cache_assoc];
			}
		}
		return NULL; // no match found, return null
	};

	// finds the victim block to replace - prioritises non-valid, then LRU
	CacheBlk* findVictim(uint32_t addr) {

		uint32_t indexFromAddr = (addr<<(tagsize))>>(32-indexsize);	// compute index of the address

		CacheBlk* oldestAccessed = &blocks[indexFromAddr*assoc];	// set initial value of least accessed block to the first one in set
		for (uint32_t i=0; i<assoc; i++) {
			if (!blocks[i+indexFromAddr*assoc].valid) { // if found a non-valid block, can immediately return
				return &blocks[i+indexFromAddr*assoc];
			} else if (blocks[i+indexFromAddr*assoc].lastAccessed < oldestAccessed->lastAccessed) {	// if current block has been last accessed earlier than the currently oldest one
				oldestAccessed = &blocks[i+indexFromAddr*assoc];
			}
		}
		return oldestAccessed;
	};

	// inserts a block specified by addr at the location of blk
	void insertBlock(uint32_t addr, CacheBlk* blk, uint32_t curCycle, bool fromPref) {
		blk->valid = true;
		blk->dirty = false;
		blk->fromPref = fromPref;	// whether the block was prefetched or not (for Next-N-Prefetch policy 2)
		blk->tag = addr>>(32-tagsize);
		blk->lastAccessed = curCycle;
	};
};

// ADDED CODE END

enum CacheRequestType
{
	CacheReadReq,
	CacheWriteReq,
	CacheWriteBackReq,
	CachePrefetchReq
};

struct CachePacket
{
	uint32_t pc;
	uint32_t addr;
	CacheRequestType reqType;		//0: read, 1: write, 2: prefetch
	int size;						//1: BYTE 4: DWORD
	uint32_t data;
	uint32_t readyCycleCount;

	CachePacket(uint32_t curCycle, uint32_t _pc, uint32_t _addr, CacheRequestType _reqType, int _size, uint32_t _data=0)
	{
		pc = _pc;
		addr=_addr;
		reqType=_reqType;
		size=_size;
		data=_data;

		readyCycleCount = curCycle;
	}
};

class CacheCtrl {
private:
	cpu_core* core;
	CachePacket* pendingMissReq;
	SimpleCache* cache;	// ADDED: allows access to cache in cache.cc
	int prefType;		// ADDED: prefetcher type
	
	//We did not support multiple outstanding misses here. 
	//- std::deque<CachePacket*> missQueue; 
	
	std::deque<CachePacket*> requestQueue;
	std::deque<PrefetchReq*> prefetchQueue;	// ADDED: prefetch queue
	std::map<uint32_t, RPTVal> RPT;			// ADDED: RPT for Stride prefetcher
	
	uint32_t hits;
	uint32_t misses;
	
	void responseToCPU(CachePacket *pkt);
	void handleCacheFill(CachePacket *pkt);	
	
public:
	CacheCtrl(cpu_core *c)
	{
		core = c;
		pendingMissReq=NULL;
		prefType=prefetcher_type;	// ADDED: prefetcher type
		hits=0;
		misses=0;
		
		//	<CAR-PA2-REF> Initialize your cache here!!
		if (cache_size>0) {
			printf("Cache Config: Size = %d b, Assoc = %d, Line Size = %d b, Prefetcher = %s\n", cache_size, cache_assoc, cacheline_size, prefetcher_type > 0 ?"enabled":"disabled");
			cache = new SimpleCache(cache_size, cache_assoc, cacheline_size);
		} else {
			cache = NULL;
		}
	}
	~CacheCtrl()
	{
		if (cache)
			delete cache;
	}
	void cycle();
	void access(CachePacket *pkt);
	void nextNPrefetch(uint32_t addr, uint32_t readyCycle);		// ADDED: next-N prefetching
	void stridePrefetch(uint32_t addr, uint32_t readyCycle);	// ADDED: stride prefetcher

	void printStat();
};

#endif /* _CACHE_H_ */
