#include "cpu.h"
#include "stages.h"
#include <cstdio>
#include <cassert>

extern uint32_t cacheline_size;	// ADDED: cache line size for getting next prefetch address
extern int lines_prefetched;	// ADDED: N for Next-N-Line prefetch (command line argument)
extern int policy;				// ADDED: prefetching policy (command line argument)

const uint32_t CACHE_MISS_NUMBER_OF_STALLS = 4;	// 4 stall cycles for cache miss (5 cycles)

void CacheCtrl::cycle()
{
	if (!requestQueue.empty())
	{
		assert(requestQueue.size()==1);			//in this simple pipeline simulator, we can only have one request from MEM stage.
		
		//<CAR-PA2-REF> reference code for handling access requests
		if (cache) {	// ADDED: flag to execute this only if there is an actual cache

			CachePacket* pkt = requestQueue.front();

			CacheBlk *blk=cache->access(pkt->addr, pkt->readyCycleCount, true); // MODIFIED: add readyCycleCount param to set lastAccessed, boolean flag to verify actual access

			if (blk && blk->isValid()) {

				// ADDED: PREFETCHING HOOK: POLICIES 2 & 3
				if ((prefType == 1) && ((policy == 3) || ((policy == 2) && (blk->fromPref)) )) {
					// if prefetching, add after cycle of last prefetch element
					if (!prefetchQueue.empty()) nextNPrefetch(pkt->addr, prefetchQueue.back()->readyCycle);
					// otherwise memory is not used, can directly fetch stuff (if (prefetchQueue.empty() && pendingMissReq) cannot happen)
					else nextNPrefetch(pkt->addr, core->cycles);
				}

				responseToCPU(pkt);
				hits++;
				requestQueue.pop_front();

			} else {
				//If there is a pendingMissReq, we should retry it in the next cycle.
				//This will not happen before you implement the prefetcher.
				if (pendingMissReq == NULL) {		//only support one outstanding miss request.
					if (prefetchQueue.empty()) {	// ADDED: always handle miss if not prefetching
						//handle cache miss
						pendingMissReq=pkt;
						pendingMissReq->readyCycleCount += CACHE_MISS_NUMBER_OF_STALLS;

						requestQueue.pop_front();
						misses++;
						if (prefType == 1) nextNPrefetch(pendingMissReq->addr, pendingMissReq->readyCycleCount); // ADDED: PREFETCHING HOOK: POLICY 1

					// ADDED: if prefetching, and there is a request on a currently prefetching block, it does not count as a new MEM request
					// hence make the block be handled as a miss fetch instead of a prefetch
					} else if (pkt->addr == prefetchQueue.front()->addr) {
						pendingMissReq=pkt;
						pendingMissReq->readyCycleCount = prefetchQueue.front()->readyCycle; 	// readyCycle may be less than the usual # of stalls
						prefetchQueue.pop_front();												// remove request from prefetchQueue

						requestQueue.pop_front();
						misses++;
						if (prefType == 1) nextNPrefetch(pendingMissReq->addr, pendingMissReq->readyCycleCount); // ADDED: PREFETCHING HOOK: POLICY 1

					}
					// otherwise we cannot do another MEM request - do not pop the CachePacket and stall until prefetchQueue is done
				}
			}

			// ADDED: STRIDE PREFETCHER HOOK
			if (prefType == 2) {
				uint32_t pc = pkt->pc;
				// if PC recorded and therefore previously accessed, set stride and new address
				if (RPT.find(pc) != RPT.end()) {
					RPT[pc].stride = pkt->addr - RPT[pc].addr;
					RPT[pc].addr = pkt->addr;
					// if stride is non-null, prefetch depending on the current queue
					if (RPT[pc].stride != 0) {
						if (!prefetchQueue.empty()) stridePrefetch(pkt->addr + RPT[pc].stride, prefetchQueue.back()->readyCycle);
						else stridePrefetch(pkt->addr + RPT[pc].stride, pkt->readyCycleCount);
					}
				// otherwise set initial values
				} else {
					RPT[pc].addr = pkt->addr;
					RPT[pc].stride = 0;
				}
			}
		}
	}

	if (pendingMissReq && (core->cycles >= pendingMissReq->readyCycleCount) ) {
		handleCacheFill(pendingMissReq);
		responseToCPU(pendingMissReq);
		pendingMissReq=NULL;
	}

	// ADDED: PREFETCHING END HOOK
	if (!prefetchQueue.empty() && (core->cycles >= prefetchQueue.front()->readyCycle)) {
		CacheBlk *victimBlk=cache->findVictim(prefetchQueue.front()->addr);				// find block to replace with the prefetched one
		cache->insertBlock(prefetchQueue.front()->addr, victimBlk, core->cycles, true);	// insert prefetched block
		prefetchQueue.pop_front();														// prefetch done, remove from queue
	}
}

// ADDED: nextNPrefetch function: takes address and readyCycle after which we should start prefetch
void CacheCtrl::nextNPrefetch(uint32_t addr, uint32_t readyCycle)
{
	// first save initial next block address and current cycles
	uint32_t nextAddr = addr + cacheline_size;
	uint32_t nextCycles = readyCycle + CACHE_MISS_NUMBER_OF_STALLS;

	// now iterate through as many blocks as we need to prefetch (limited of line_prefetched entries in queue at any time)
	int curSize = prefetchQueue.size();
	for (int i=0; i<lines_prefetched-curSize; i++) {

		CacheBlk *nxtblk = cache->access(nextAddr, 0, false); // check if the block at the next address is in cache, without changing access time

		if (nxtblk && nxtblk->isValid()) {
			// block already in cache, no prefetching needed
		} else {										// otherwise make prefetch request with modified pendingMissReq parameters
			PrefetchReq* p = new PrefetchReq;
			p->addr = nextAddr;
			p->readyCycle = nextCycles;
			prefetchQueue.push_back(p);					// add request to queue
			nextCycles+=CACHE_MISS_NUMBER_OF_STALLS;	// increment nextCycles only if already found a block for fetching
		}
		nextAddr+=cacheline_size; 	// increment nextAddr in any case
	}
}

// ADDED: prefetching for Stride
void CacheCtrl::stridePrefetch(uint32_t addr, uint32_t readyCycle)
{
	CacheBlk *nxtblk = cache->access(addr, 0, false); // check if the block at the address is in cache, without changing access time

	if (nxtblk && nxtblk->isValid()) {
		// block already in cache, no prefetching needed
	} else {
		PrefetchReq* p = new PrefetchReq;
		p->addr = addr;
		p->readyCycle = readyCycle + CACHE_MISS_NUMBER_OF_STALLS;
		prefetchQueue.push_back(p);
	}
}

void CacheCtrl::access(CachePacket *pkt)
{	
	if (cache) {
		requestQueue.push_back(pkt);
	} else {					//no cache, always miss.
		pendingMissReq=pkt;
		pendingMissReq->readyCycleCount += CACHE_MISS_NUMBER_OF_STALLS;
		misses++;
	}
}

void CacheCtrl::handleCacheFill(CachePacket *pkt)
{	
	//<CAR-PA2-REF> reference code for handling cache fill

	if (cache) {	// ADDED: flag to execute this only if there is an actual cache

		CacheBlk *victimBlk=cache->findVictim(pkt->addr);

		if (victimBlk->isValid() && victimBlk->isDirty()) {
			//Theoretically, cache will issue a writeback request to the memory,
			//but you can assume ignore this writeback here.
			//issueWriteBack();
		}

		cache->insertBlock(pkt->addr,victimBlk,pkt->readyCycleCount, false); // MODIFIED: add the readyCycleCount param to set lastAccessed, flag for prefetching policy 2

		if (pkt->reqType == CacheWriteReq)
			victimBlk->markDirty();
	}
}

void CacheCtrl::responseToCPU(CachePacket *pkt)
{
	//NOTE: You do not need to simulate the data in the cache.
	//This is a hack for you to get the data from memory.
	if (pkt->reqType == CacheWriteReq) {
		if (pkt->size == 1) {
			core->mem->set<byte>(pkt->addr, pkt->data);
		} else if (pkt->size == 4) {
			core->mem->set<uint32_t>(pkt->addr, pkt->data);
		} else {
			assert(1);
		}
	} else 	if (pkt->reqType == CacheReadReq) {
		if (pkt->size == 1) {
			pkt->data = core->mem->get<byte>(pkt->addr);
		} else if (pkt->size == 4) {
			pkt->data = core->mem->get<uint32_t>(pkt->addr);
		} else {
			assert(1);
		}
	} else {
		assert(1);
	}
	
	core->mys.response(pkt);
}

void CacheCtrl::printStat()
{
	//print the statistic here
	printf("stat.dcache_hits: %d\n", hits);
	printf("stat.dcache_misses: %d\n", misses);
	printf("stat.dcache_miss_rate: %.2f %%\n", ((double)misses/(double)(hits+misses)) * 100);	
}
