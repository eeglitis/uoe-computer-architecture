#include "cpu.h"
#include "stages.h"
#include <assert.h>
#include <stdio.h>
#define ADDR(X) (X & 0x1FF8) >> 3 	// gets index for 2-bit predictor
#define ADDRGHR(X) (X & 0x3FF)		// gets index for 2-level predictor

// the three operands are encoded inside a single uint16_t. This method cracks them open
static void inline decode_ops(uint16_t input, byte *dest, byte *src1, byte *src2)
{
	*dest = input & 0x001F;
	*src1 = (input & 0x03E0) >> 5;
	*src2 = (input & 0x7C00) >> 10;
}

static bool inline isBranch(IDl latch)
{
	return latch.control()->branch;
}

/**
   The execute implementations. These actually perform the action of the stage.
 **/

// Instructions are fetched from memory in this stage, and passed into the CPU's ID latch
void InstructionFetchStage::Execute()
{
	if (OBF) {
		return;
	}
	IBF=false;
	
	right.opcode = core->mem->get<byte>(core->PC);
	uint16_t operands = core->mem->get<uint16_t>(core->PC + 1);
	decode_ops(operands, &right.Rdest, &right.Rsrc1, &right.Rsrc2);
	right.immediate = core->mem->get<uint32_t>(core->PC + 3);

	// <CAR_PA1_HOOK2> start

	// !!! Assume an oracle BTB; i.e, we know if the current instruction is a branch or not and its target address!!!
	// !!! Taken branch target address is stored in "right.immediate"!!!

	if (isBranch(right)) {
		switch(core->predtype) {

			// Always Taken
			case 1: {
				right.predict_taken = true;
				right.recoveryPC = core->PC+8;
				core->PC = right.immediate;
			}
			break;

			// 2-Bit Predictor
			case 2: {
				if (core->predictor[ADDR(core->PC)] > 1) { // if counter is 2 (10) or 3 (11), predict taken
					right.predict_taken = true;
					right.recoveryPC = core->PC+8;
					core->PC = right.immediate;
				} else { // predict not taken
					right.predict_taken = false;
					core->PC += 8;
					right.recoveryPC = core->PC; // will provide persistent fallback to branch address
				}
			}
			break;

			// 2-Level Predictor
			case 3: {
				right.recoveryGHR = core->GHR; // save a copy of the current GHR to propagate
				if (core->predictor[ADDRGHR(core->GHR)] > 1) { // if counter is 2 (10) or 3 (11), predict taken
					core->GHR = ((core->GHR)<<1) | 1;
					right.predict_taken = true;
					right.recoveryPC = core->PC+8;
					core->PC = right.immediate;
				} else { // predict not taken
					core->GHR = (core->GHR)<<1;
					right.predict_taken = false;
					core->PC += 8;
					right.recoveryPC = core->PC; // provides persistent fallback to branch address
				}
			}
			break;

			// Always Not Taken
			default: {
				right.predict_taken = false;
				core->PC += 8;
			}
		}
	} else {
		right.predict_taken = false;
		core->PC += 8;
	}

	// <CAR_PA1_HOOK2> end
	
	OBF=true;
}

void InstructionDecodeStage::Execute()
{
	if (!IBF || OBF) {	//no input or output is not read by the next stage
		return;
	}
	IBF=false;
	
	core->registers[0].value = 0; // wire register 0 to zero for all register reads
	right.Rsrc1Val = *(int32_t*) &core->registers[left.Rsrc1].value;
	right.Rsrc2Val = *(int32_t*) &core->registers[left.Rsrc2].value;
	right.immediate = left.immediate;
	right.Rsrc1 = left.Rsrc1;
	right.Rsrc2 = left.Rsrc2;
	right.Rdest = left.Rdest;
	right.opcode = left.opcode;
	right.predict_taken = left.predict_taken;
	right.recoveryPC = left.recoveryPC;
	right.recoveryGHR = left.recoveryGHR;

	if (right.Rsrc1 && core->registers[right.Rsrc1].lockRefCount) {
		//printf("Rsrc1 (%d) is locked, stall ?\n", right.Rsrc1);
		right.setRsrc1Ready(false);

	} else {
		right.setRsrc1Ready(true);
	}

	if (right.Rsrc2 && core->registers[right.Rsrc2].lockRefCount) {
		//printf("Rsrc2 (%d) is locked, stall ?\n", right.Rsrc2);
		right.setRsrc2Ready(false);
	} else {
		right.setRsrc2Ready(true);
	}

	//lock destination register
	if (right.Rdest && right.control()->register_write) {
		core->registers[right.Rdest].lockRefCount++;
	}

	OBF=true;
}


void ExecuteStage::Execute()
{
	if (!IBF || OBF) {	//no input or output is not read by the next stage
		return;
	} else {
		if (busyCycles==0)
		{
			// copy forward from previous latch
			right.opcode = left.opcode;
			right.Rsrc1 = left.Rsrc1;
			right.Rsrc2 = left.Rsrc2;
			right.Rsrc1Val = left.Rsrc1Val;
			right.Rsrc2Val = left.Rsrc2Val;
			right.Rdest = left.Rdest;
			//load execution cycles
			busyCycles = left.control()->exe_cycles;
		}
	}
	
	// execution stage start
	--busyCycles;
	if (busyCycles) {		//return if we need to wait for more cycles
		//printf("Exe Stage Stall: Wait %d cycles\n",busyCycles);	
		return;
	} 
	//set IBF to 0;
	IBF=false;
		
	uint32_t param;
	
	switch (left.control()->alu_source) {
	case 0: // source from register
		param = left.Rsrc2Val;
		break;

	case 1: // immediate add/sub
		param = left.immediate;
		break;

	case 2: // address calculation
		param = left.immediate + *(uint32_t *)&left.Rsrc1Val;
		break;
	}
	// for those operands which require signed arithmetic.
	int32_t svalue = left.Rsrc1Val;
	int32_t sparam = *(int32_t *)&param;
	int32_t result = sparam;

	if (left.control()->branch) {
		bool taken = (left.opcode == 2 && left.Rsrc1Val == 0) ||
		             (left.opcode == 3 && left.Rsrc1Val >= left.Rsrc2Val) ||
		             (left.opcode == 4 && left.Rsrc1Val != left.Rsrc2Val);

		// if mispredict, nop out IF and ID. (mispredict == prediction and taken differ)
		if (core->verbose) printf(taken ? "taken %d  %d\n" : "nottaken %d  %d\n", left.Rsrc1Val, left.Rsrc2Val);

		if (left.predict_taken != taken) {
			if (core->verbose) printf("\033[32m*** MISPREDICT!\033[0m\n");
			core->ifs.make_nop();
			core->ids.make_nop();
			core->BPMisses++;

			switch (core->predtype) { // changes PC, updates predictor for mispredicts

				// Always Taken - branch actually not taken, load original PC+8
				case 1: core->PC = left.recoveryPC;
				break;

				// 2-Bit Predictor
				case 2: {
					if (left.predict_taken) { // predicted taken, so PC was changed to branch target - go to recoveryPC
						core->predictor[ADDR(left.recoveryPC-8)]--;
						core->PC = left.recoveryPC;
					} else { // predicted not taken, so PC was incremented - go to target branch address
						core->predictor[ADDR(left.recoveryPC-8)]++;
						core->PC = left.immediate;
					}
				}
				break;

				// 2-Level Predictor
				case 3: {
					if (left.predict_taken) { // predicted taken, actually not taken - decrement counter at old GHR
						core->predictor[ADDRGHR(left.recoveryGHR)]--;
						core->GHR = (left.recoveryGHR)<<1; // shift in GHR as not taken
						core->PC = left.recoveryPC;
					} else { // predicted not taken, actually taken - increment counter at old GHR
						core->predictor[ADDRGHR(left.recoveryGHR)]++;
						core->GHR = ((left.recoveryGHR)<<1) | 1; // shift in GHR as taken
						core->PC = left.immediate;
					}
				}
				break;

				// Always Not Taken - branch actually taken, load branch target
				default: core->PC = left.immediate;
			}
		} else {
			core->BPHits++;

			switch (core->predtype) { // updates predictor for correct predicts

				// 2-Bit Predictor
				case 2: {
					if (taken) {
						core->predictor[ADDR(left.recoveryPC-8)] = 3; // predicted taken + actually taken = counter set to 11
					} else {
						core->predictor[ADDR(left.recoveryPC-8)] = 0; // predicted not taken + actually not taken = counter set to 00
					}
				}
				break;

				// 2-Level Predictor
				case 3: {
					if (taken) {
						core->predictor[ADDRGHR(left.recoveryGHR)] = 3; // predicted taken + actually taken = counter set to 11
					} else {
						core->predictor[ADDRGHR(left.recoveryGHR)] = 0; // predicted not taken + actually not taken = counter set to 00
					}
				}
				break;

				// Always Taken / Always Not Taken - nothing to change
				default: break;
			}
		}
	}

	switch (left.control()->alu_operation) {
	case 0:
		// do nothing, this operation does not require an alu op (copy forward)
		break;

	case 1:
		// do a signed add of reg1 to param
		result = svalue + sparam;
		break;

	case 2:
		// do a signed subtract of param from reg1
		result = svalue - sparam;
		break;
	}
	right.aluresult = *(uint32_t *)&result;
	OBF=true;	
}


void MemoryStage::Execute()
{
	if (!IBF || OBF) {	//no input or output is not read by the next stage
		return;
	}
	IBF=false;	

	const instruction *control = left.control();

	right.aluresult = left.aluresult;
	right.mem_data = 0;
	if (control->mem_read) {
		if (control->mem_read == 1) {
			right.mem_data = core->mem->get<byte>(left.aluresult);
		}
		else if (control->mem_read == 4) {
			right.mem_data = core->mem->get<uint32_t>(left.aluresult);
		}
	}
	else if (control->mem_write) {
		if (control->mem_write == 1) {
			core->mem->set<byte>(left.aluresult, left.Rsrc2Val);
		}
		else if (control->mem_write == 4) {
			core->mem->set<uint32_t>(left.aluresult, left.Rsrc2Val);
		}
	}

	right.opcode = left.opcode;
	right.Rsrc1Val = left.Rsrc1Val;
	right.Rsrc2Val = left.Rsrc2Val;
	right.Rdest = left.Rdest;
	OBF=true;
}


void WriteBackStage::Execute()
{
	if (!IBF) {	//no input 
		return;
	}
	IBF=false;
	
	const instruction *control = left.control();

	if (control->special_case != NULL) {
		control->special_case(core);
	}
	if (control->register_write) {
		if (control->mem_to_register) {
			// write the mem_data into register Rdest
			core->registers[left.Rdest].value = left.mem_data;
		}
		else {
			// write the alu result into register Rdest
			core->registers[left.Rdest].value = left.aluresult;
		}
		core->registers[0].value = 0; // wire back to zero
		assert(core->registers[left.Rdest].lockRefCount>0);
		core->registers[left.Rdest].lockRefCount--;	
	}
}


void InstructionDecodeStage::Shift()
{
	if (IBF || OBF || !right.isReady())		//if OBF is set or data is not ready in the right latch, we cannot fetch from the previous stage
		return;

	if (core->ifs.OBF && core->ifs.right.isReady()) {
		left = core->ifs.right;
		core->ifs.OBF=false;
	} else {
		//create a nop if the previous stage is not ready
		left.reset();
	}
	IBF=true;	
}


void ExecuteStage::Shift()
{
	//if (busyCycles>0)
	//	return;

	if (IBF || OBF || !right.isReady())		//if OBF is set or data is not ready in the right latch, we cannot fetch from the previous stage
		return;
	
	if (core->ids.OBF && core->ids.right.isReady()) {
		left = core->ids.right;
		core->ids.OBF=false;
	} else {
		//create a nop if the previous stage is not ready
		left.reset();
	}
	IBF=true;	
}


void MemoryStage::Shift()
{
	if (IBF || OBF || !right.isReady())		//if OBF is set or data is not ready in the right latch, we cannot fetch from the previous stage
		return;
	
	if (core->exs.OBF && core->exs.right.isReady()) {
		left = core->exs.right;
		core->exs.OBF=false;
	} else {
		//create a nop if the previous stage is not ready		
		left.reset();
	}
	IBF=true;

}


void WriteBackStage::Shift()
{
	if (IBF)
		return;
	
	if (core->mys.OBF && core->mys.right.isReady()) {
		left = core->mys.right;
		core->mys.OBF=false;
		IBF=true;		
	} else {
		//create a nop if the previous stage is not ready
		left.reset();
	}
	IBF=true;	
}

void ExecuteStage::DoForwarding()
{
	if (busyCycles>0)
		return;
	
	// If the previous instruction is attempting a READ of the same register the instruction
	// in this stage is supposed to WRITE, then here, update the previous stage's right latch
	// with the value coming out of the ALU. (also, not reading zero reg)
	if (!core->exs.right.control()->mem_read && core->exs.right.Rdest != 0 && core->exs.right.control()->register_write) {
		if (core->exs.right.Rdest == core->ids.right.Rsrc1) {
			core->ids.right.Rsrc1Val = core->exs.right.aluresult;
			core->ids.right.setRsrc1Ready(true);
			if (core->verbose) printf("\033[34m*** FORWARDex1\033[0m:  %08x going to ID/EX's Rsrc1Val \n",
				                       core->exs.right.aluresult);
		}
		if (core->exs.right.Rdest == core->ids.right.Rsrc2) {
			core->ids.right.Rsrc2Val = core->exs.right.aluresult;
			core->ids.right.setRsrc2Ready(true);
			if (core->verbose) printf("\033[34m*** FORWARDex2\033[0m:  %08x going to ID/EX's Rsrc2Val \n",
				                       core->exs.right.aluresult);
		}
	}
}


void MemoryStage::DoForwarding()
{
	// If the next to previous instruction (IDS) is attempting a READ of the same register the instruction
	// in this stage is supposed to WRITE, then here, update the next-to-previous stage's right latch
	// with the value coming out of the this stage. (also, the read is not on zero)
	if (core->mys.right.Rdest != 0 && core->mys.right.control()->register_write) {
		if (core->exs.right.Rdest != core->ids.right.Rsrc1 &&
		    core->mys.right.Rdest == core->ids.right.Rsrc1) {
			core->ids.right.Rsrc1Val =
			   core->mys.right.control()->mem_read ? core->mys.right.mem_data : core->mys.right.aluresult;
			core->ids.right.setRsrc1Ready(true);
		
			if (core->verbose) printf("\033[34m*** FORWARDmem1\033[0m: %08x going to ID/EX's Rsrc1Val \n",
				                       core->ids.right.Rsrc1Val);
		}
		if (core->exs.right.Rdest != core->ids.right.Rsrc2 &&
		    core->mys.right.Rdest == core->ids.right.Rsrc2) {
			core->ids.right.Rsrc2Val =
			   core->mys.right.control()->mem_read ? core->mys.right.mem_data : core->mys.right.aluresult;
			core->ids.right.setRsrc2Ready(true);
		
			if (core->verbose) printf("\033[34m*** FORWARDmem2\033[0m: %08x going to ID/EX's Rsrc2Val \n",
				                       core->ids.right.Rsrc2Val);
		}
	}
}

void InstructionDecodeStage::make_nop()
{
	if (right.Rdest && right.control()->register_write) {
		core->registers[right.Rdest].lockRefCount--;
		//printf("Lock dest register:R%d ref:%d",right.Rdest,core->registers[right.Rdest].lockRefCount)
	}

	left.reset();
	right.reset();
	right.setRsrc1Ready(true);
	right.setRsrc2Ready(true);
}

