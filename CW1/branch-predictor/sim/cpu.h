#ifndef _CPU_H_
#define _CPU_H_
#include "memory.h"
#include "instruction.h"
#include "stages.h"

struct RegisterStruct
{
	int lockRefCount;
	uint32_t value;
	RegisterStruct()
	{
		lockRefCount=0;
		value=0;
	}
};

// Register machine core state.
class cpu_core {
public:
	cpu_core() : ifs(this), ids(this), exs(this), mys(this), wbs(this) {}

	uint32_t PC;
	uint32_t cycles;
	uint32_t BPHits; 
	uint32_t BPMisses; 
	bool usermode, verbose;
	memory  *mem;
	//uint32_t registers[32];
	RegisterStruct registers[32];
	InstructionFetchStage  ifs;
	InstructionDecodeStage ids;
	ExecuteStage   exs;
	MemoryStage    mys;
	WriteBackStage wbs;
	int predtype;			// predictor type (0 to 3)
	byte predictor[1024];
	uint16_t GHR;			// global history register
};

void run_cpu(memory *m, const bool verbose, int pred_type);

#endif /* _CPU_H_ */
